import 'package:flutter/material.dart';
import 'package:hr_management_deploy/bloc/daftar_rencana-bloc.dart';
import 'package:hr_management_deploy/page/login.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:provider/provider.dart';
import 'bloc/approve_rencana_bloc.dart';
import 'bloc/user-bloc.dart';


void main() => runApp(
   MultiProvider(
    providers: [
      ChangeNotifierProvider<UserBlock>.value(value: UserBlock(),),
      ChangeNotifierProvider<DaftarRencanaBlock>.value(value: DaftarRencanaBlock(),),
      ChangeNotifierProvider<ApproveRencanaBlock>.value(value: ApproveRencanaBlock(),),
    ],
      child: MaterialApp(
        title: "SISFO",
        debugShowCheckedModeBanner: false,
        home: SplashPage(),
      ),
    ));

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 3,
      navigateAfterSeconds: new LoginPage(),
      title: new Text(
        'HR-MAN',
        style: new TextStyle(
            // fontWeight: FontWeight.bold,
            fontSize: 12.0),
      ),
      image: new Image.asset(
        'assets/hr.png',
        width: 150,
        height: 150,
      ),
      backgroundColor: Colors.white,
      styleTextUnderTheLoader: new TextStyle(),
      loadingText: Text(
        "HR-MANAGEMENT SYSTEM \n ©2019 v.0.1 Integrated ERP SAP",
        textAlign: TextAlign.center,
      ),
      photoSize: 40.0,
      onClick: () => print("Flutter Egypt"),
      loaderColor: Colors.red,
    );
  }
}
