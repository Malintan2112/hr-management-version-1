import 'package:animator/animator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hr_management_deploy/bloc/approve_rencana_bloc.dart';
import 'package:hr_management_deploy/bloc/daftar_rencana-bloc.dart';
import 'package:hr_management_deploy/bloc/user-bloc.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../home.dart';

class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

String idToken = "";

class _WelcomeState extends State<Welcome> {
  getApiToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idToken = preferences.getString("idToken");
      print("id token $idToken");
    });
  }

  UserBlock userBlock;
  DaftarRencanaBlock drb;
  ApproveRencanaBlock arb;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getApiToken();
    userBlock = Provider.of<UserBlock>(context, listen: false);
    drb = Provider.of<DaftarRencanaBlock>(context, listen: false);
    arb = Provider.of<ApproveRencanaBlock>(context, listen: false);


  }

  String welcome = "Hi ! I'm HR MANAGEMENT ";
  String text_first = "I Already Know";
  Color one = Color(0xFFf45d27);
  Color two = Color(0xFFf5851f);
  Widget notif = Padding(
      padding: EdgeInsets.all(90),
      child: Center(
        child: Text(
          "I will assist you in making permissions including overtime, leave, being late and being sick independently",
          textAlign: TextAlign.center,
        ),
      ));
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 50,
            ),
            Padding(
              padding: const EdgeInsets.all(50.0),
              child: Center(
                child: Text(
                  welcome,
                  style: TextStyle(fontSize: 15),
                ),
              ),
            ),
            Animator(
              duration: Duration(seconds: 5),
              builder: (anim) => Center(
                child: Transform.scale(
                  scale: anim.value,
                  child: Image.asset('assets/hr.png', width: 80, height: 80),
                ),
              ),
            ),
            notif,
            SizedBox(
              height: 60,
            ),
            GestureDetector(
              child: Container(
                height: 45,
                width: MediaQuery.of(context).size.width / 1.5,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [one, two],
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(50))),
                child: Center(
                  child: Text(
                    text_first.toUpperCase(),
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              onTap: () {
                userBlock.getDetail(idToken);
                userBlock.idToken = idToken;
                arb.idToken = idToken;
                drb.idToken = idToken;

                setState(() {
                  welcome = "Hi Welcome  !!";
                  text_first = "let's start";
                  one = Color(0xFF00C853);
                  two = Color(0xFF00E676);
                  notif = Padding(
                    padding: EdgeInsets.all(100),
                    child: CircularProgressIndicator(),
                  );
                });
                if (userBlock.detailuser.id_token == idToken) {
                  print(userBlock.idToken);
                  userBlock.idArea = userBlock.detailuser.id_area;
                  if (userBlock.idArea == userBlock.detailuser.id_area) {
                    userBlock.fetchKasubag();
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext) => HomePage()));
                  } else {
                    userBlock.idArea = userBlock.detailuser.id_area;
                  }
                } else {
                  userBlock.getDetail(idToken);
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
