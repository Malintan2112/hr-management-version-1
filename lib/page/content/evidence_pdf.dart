import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';

class PDFViewPage extends StatefulWidget {
  final String path;

  const PDFViewPage({Key key, this.path}) : super(key: key);
  @override
  _PDFViewPageState createState() => _PDFViewPageState();
}

class _PDFViewPageState extends State<PDFViewPage> {
  bool pdfReady = false;
  int _totalPage = 0;
  int _currentPage = 0;
  PDFViewController _pdfViewController;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lihat Detail "),
      ),
      body: Stack(
        children: <Widget>[
          PDFView(
            filePath: widget.path,
            autoSpacing: true,
            enableSwipe: true,
            pageSnap: true,
            swipeHorizontal: true,
            onError: (e) {
              print(e);
            },
            onRender: (_pages) {
              setState(() {
                _totalPage = _pages;
                pdfReady = true;
              });
            },
            onViewCreated: (PDFViewController vc) {
              _pdfViewController = vc;
            },
            onPageChanged: (int page, int total) {
              setState(() {});
            },
            onPageError: (page, e) {},
          ),
          !pdfReady ? Column(children: <Widget>[
            Padding(padding: EdgeInsets.all(50.0),),
            CircularProgressIndicator() 
          ],): Offstage(),
        ],
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          _currentPage >0?FloatingActionButton.extended(
            backgroundColor: Colors.red,
            label:  Text("Ke Halaman ${_currentPage-1}"),
            onPressed: (){
              _currentPage -=1;
              _pdfViewController.setPage(_currentPage);
            },
          ):Offstage(),
          _currentPage <_totalPage?FloatingActionButton.extended(
            backgroundColor: Colors.green,
            label:  Text("Ke Halaman ${_currentPage+1}"),
            onPressed: (){
              _currentPage +=1;
              _pdfViewController.setPage(_currentPage);
            },
          ):Offstage(),
        ],
      ),
    );
  }
}
