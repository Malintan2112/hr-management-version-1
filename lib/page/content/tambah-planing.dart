import 'dart:convert';
import 'dart:io';
import 'package:animator/animator.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:hr_management_deploy/bloc/user-bloc.dart';
import 'package:hr_management_deploy/model/KasubagModel.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'daftar-planing.dart';
import 'package:async/async.dart';
import 'package:path/path.dart';

class FormTPPage extends StatefulWidget {
  @override
  _FormTPPageState createState() => _FormTPPageState();
}

class _FormTPPageState extends State<FormTPPage> {
  List<String> opsi = [];
  List<String> opsi_cuti_array = ['CUTI TAHUNAN', 'CUTI PANJANG'];
  List<String> opsiApprovalKabag = [];
  List<Kasubag> opsiApprovalKaSubbag = [];
  String _opsi = "";
  String _opsiApprovalKabag;
  String _opsiApprovalKaSubbag;
  final format = DateFormat("dd-MM-yyyy");
  Widget waktu_awal = SizedBox();
  Widget waktu_akhir = SizedBox();
  Widget opsi_cuti = SizedBox();
  Widget _decideImageView() {
    if (imageFile == null) {
      return Text(
        "Tambahkan bukti untuk memperkuat rencana",
        style: TextStyle(fontStyle: FontStyle.italic, color: Colors.black87),
      );
    } else {
      return Image.file(
        imageFile,
        width: 400,
        height: 400,
      );
    }
  }

  String idToken = "";
  getApiToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idToken = preferences.getString("idToken");
      print("id token $idToken");
    });
  }

  UserBlock userBlock;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getApiToken();
    // this.getSWData();
  }

  File imageFile = null;

  addData(
      int id_user,
      String nik,
      String tgl_mulai,
      String tgl_akhir,
      int area,
      int subArea,
      int status,
      int statusCuti,
      String opsiapproval,
      File image,
      BuildContext context) async {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Mohon tunggu ....."),
    ));

    var url = Uri.parse(
        "http://dash.ptpnix.co.id/hrsistems/api/fungsiaddplanner?api_token=${userBlock.detailuser.id_token}");
    var request = new http.MultipartRequest("POST", url);

    request.fields['id_user'] = id_user.toString();
    request.fields['nik'] = nik.toString();
    request.fields['tgl_mulai'] = tgl_mulai;
    request.fields['tgl_akhir'] = tgl_akhir;
    request.fields['area'] = area.toString();
    request.fields['sub_area'] = subArea.toString();
    request.fields['status'] = status.toString();
    // request.fields['statuscuti'] =(statusCuti!=null)?status.toString():"" ;
    request.fields['statuscuti'] = "0";
    if (statusCuti != null) {
      request.fields['statuscuti'] = status.toString();
    }
    request.fields['keterangan'] = keterangan.text;
    request.fields['opsiapproval'] = opsiapproval;
    if (image != null) {
      var stream =
          new http.ByteStream(DelegatingStream.typed(image.openRead()));
      var length = await image.length();
      var multipartFile = new http.MultipartFile("file", stream, length,
          filename: basename(image.path));
      request.files.add(multipartFile);
    }
    // print("ini url ($url)");
    // final response = await http.post(url,
    //     body: {
    //       "id_user": "${id_user}",
    //       "nik": "${nik}",
    //       "tgl_mulai": "${tgl_mulai}",
    //       "tgl_akhir": "${tgl_akhir}",
    //       "area": "${area}",
    //       "subarea": "${subArea}",
    //       "status": '${status}',
    //       "statuscuti": '${statusCuti}',
    //       "keterangan": "${keterangan}",
    //       "opsiapproval": "${opsiapproval}",
    //       "file": "",
    //     },
    //     encoding: Encoding.getByName("utf-8"));
    var response = await request.send();
    // var data = jsonDecode(response.statusCode);

    try {
      print(response.statusCode);
      if (response.statusCode == 200) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (BuildContext) => DPPage()));
        // return DPPage();
        // return data['message'];
      } else if (response.statusCode == 500) {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Permintaan di tolak "),
          action: SnackBarAction(
            label: 'Cancel',
            textColor: Colors.red,
            onPressed: () {
              return null;
            },
          ),
        ));
      }
    } on Exception catch (error) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Terjadi Masalah Coba  ..."),
        action: SnackBarAction(
          label: 'Cancel',
          textColor: Colors.red,
          onPressed: () {
            return null;
          },
        ),
      ));
      print('erorr');
    }
  }

  int no = 0;

  String _mySelection;
  int status;
  int id_kabag;
  int statusCuti;
  String tgl_mulai;
  String tgl_akhir;
  TextEditingController keterangan = new TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  var data;
  @override
  Widget build(BuildContext context) {
    userBlock = Provider.of<UserBlock>(context, listen: false);

    int id_user = userBlock.detailuser.id_user;
    String nik = userBlock.detailuser.nik;

    int area = userBlock.detailuser.id_area;
    int subArea = userBlock.detailuser.id_subarea;

    String opsiapproval;
    String file = null;
    userBlock.idToken = idToken;
    if (userBlock.detailuser.id_jabatan == 301 ||
        userBlock.detailuser.id_jabatan == 202 ||
        userBlock.detailuser.id_jabatan == 203) {
      opsi = ['MEMO TERLAMBAT', 'SAKIT', 'CUTI', 'DISPENSASI'];
    } else {
      opsi = ['MEMO TERLAMBAT', 'SAKIT', 'CUTI', 'DISPENSASI', 'LEMBUR'];
    }
    opsiApprovalKabag = ['${userBlock.detailuser.nama_kabag}'];
    // opsiApprovalKaSubbag = ['${userBlock.detailuser.nama_kasubag}'];
    Widget form = Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 20.0),
        ),
        DropDownField(
            // enabled: false,
            // controller: opsiIjin,
            itemsVisibleInDropdown: 5,
            hintText: "Masukan rencana mu",
            value: _opsi,
            // required: true,
            strict: true,
            required: true,
            onValueChanged: (dynamic newValue) {
              switch (newValue) {
                case "CUTI":
                  status = 1;
                  break;
                case "SAKIT":
                  status = 2;
                  statusCuti = null;
                  break;
                case "DINAS":
                  status = 3;
                  statusCuti = null;
                  break;
                case "MEMO TERLAMBAT":
                  status = 4;
                  statusCuti = null;
                  break;
                case "DISPENSASI":
                  status = 5;
                  statusCuti = null;
                  break;
                case "LEMBUR":
                  status = 6;
                  statusCuti = null;
                  break;
                default:
              }
              if (newValue != "MEMO TERLAMBAT") {
                setState(() {
                  waktu_awal = DateTimeField(
                    format: format,
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.calendar_today,
                        color: Colors.blueAccent,
                      ),
                      labelText: "Tanggal Mulai ..",
                    ),
                    onShowPicker: (context, currentValue) {
                      return showDatePicker(
                          context: context,
                          firstDate: DateTime(1900),
                          initialDate: currentValue ?? DateTime.now(),
                          lastDate: DateTime(2100));
                    },
                    onChanged: (DateTime value) {
                      tgl_mulai = "${value.year}-${value.month}-${value.day}";
                    },
                  );
                  waktu_akhir = DateTimeField(
                    format: format,
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.calendar_today,
                        color: Colors.blueAccent,
                      ),
                      labelText: "Tanggal Akhir ..",
                    ),
                    onChanged: (DateTime value) {
                      tgl_akhir = "${value.year}-${value.month}-${value.day}";
                    },
                    onShowPicker: (context, currentValue) {
                      return showDatePicker(
                          context: context,
                          firstDate: DateTime(1900),
                          initialDate: currentValue ?? DateTime.now(),
                          lastDate: DateTime(2100));
                    },
                  );
                });
              } else {
                setState(() {
                  DateTime now = new DateTime.now();
                  tgl_akhir = "${now.year}-${now.month}-${now.day}";
                  tgl_mulai = "${now.year}-${now.month}-${now.day}";
                  waktu_akhir = Text("");
                  waktu_awal = Text("");
                });
              }
              if (newValue == "CUTI") {
                setState(() {
                  opsi_cuti = DropDownField(
                      value: _opsiApprovalKabag,
                      // required: true,
                      strict: true,
                      labelText: 'Opsi Cuti',
                      labelStyle: TextStyle(fontSize: 15.0),
                      items: opsi_cuti_array,
                      onValueChanged: (dynamic newValue) {
                        switch (newValue) {
                          case "CUTI TAHUNAN":
                            // status = 1;
                            statusCuti = 5;
                            break;
                          case "CUTI PANJANG":
                            // status = 6;
                            statusCuti = 4;
                            break;
                          default:
                        }
                        print("ini status oi ${statusCuti}");

                        FocusScope.of(context).requestFocus(FocusNode());
                      },
                      setter: (dynamic newValue) {
                        _opsiApprovalKabag = newValue;
                      });
                });
              } else {
                setState(() {
                  opsi_cuti = Text("");
                });
              }
              FocusScope.of(context).requestFocus(FocusNode());
            },
            labelText: 'Opsi Absensi',
            labelStyle: TextStyle(fontSize: 15.0),
            items: opsi,
            textStyle: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            setter: (dynamic newValue) {
              _opsi = newValue;
              // waktu_akhir = Text(_opsi);
            }),
        Padding(
          padding: EdgeInsets.only(top: 10.0),
        ),
        opsi_cuti,
        Padding(
          padding: EdgeInsets.only(top: 5.0),
        ),
        waktu_awal,
        Padding(
          padding: EdgeInsets.only(top: 4.0),
        ),
        waktu_akhir,
        Padding(
          padding: EdgeInsets.only(top: 1.0),
        ),
        TextField(
          controller: keterangan,
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.note,
              color: Colors.blueAccent,
            ),
            hintText: "masukan keterangan ...",
            labelText: "Keterangan",
          ),
          maxLines: 3,
          keyboardType: TextInputType.multiline,
        ),
        Padding(
          padding: EdgeInsets.only(top: 20.0),
        ),
        GestureDetector(
          child: Column(
            children: <Widget>[
              Image.asset("assets/jpg.png"),
              _decideImageView(),
            ],
          ),
          onTap: () {
            _showChoiceDialog(context);
          },
        ),
        Padding(
          padding: EdgeInsets.only(top: 30.0),
        ),
        (userBlock.detailuser.id_jabatan != 203)
            ? new Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Text("KABAG",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15))
                      ],
                    ),
                  ),
                  new DropdownButton(
                    iconEnabledColor: Colors.blue,
                    hint: Text("Nama Kabag"),
                    items: opsiApprovalKabag
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        child: Container(
                            width: MediaQuery.of(context).size.width / 1.5,
                            child: new Text(value)),
                        value: value,
                      );
                    }).toList(),
                    onChanged: (newVal) {
                      id_kabag = userBlock.detailuser.id_kabag;
                      print(id_kabag);
                      setState(() {
                        _opsiApprovalKabag = newVal;
                      });
                    },
                    value: _opsiApprovalKabag,
                  ),
                ],
              )
            : Text("data"),
        Padding(
          padding: EdgeInsets.only(top: 10.0),
        ),
        (userBlock.detailuser.id_jabatan != 202)
            ? new Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Text("KASUBAG",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15))
                      ],
                    ),
                  ),
                  new DropdownButton(
                    iconEnabledColor: Colors.blue,
                    hint: Text("Nama Kasubag"),
                    items: userBlock.listkasubag.map((item) {
                      return new DropdownMenuItem(
                        child: Container(
                            width: MediaQuery.of(context).size.width / 1.5,
                            child: new Text(item.nama_kasubag)),
                        value: item.id_kasubag.toString(),
                      );
                    }).toList(),
                    onChanged: (newVal) {
                      // print(newVal);
                      setState(() {
                        _mySelection = newVal;
                      });
                    },
                    value: _mySelection,
                  ),
                ],
              )
            : Text(""),
        Padding(
          padding: EdgeInsets.only(top: 40.0),
        ),
        GestureDetector(
          child: Container(
            height: 45,
            width: MediaQuery.of(context).size.width / 3,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color(0xffaeea00), Color(0xffaeea00)],
                ),
                borderRadius: BorderRadius.all(Radius.circular(50))),
            child: Center(
              child: Text(
                'kirim '.toUpperCase(),
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          onTap: () {
            showDialog(
                context: context,
                builder: (_) => AssetGiffyDialog(
                      image: Image.asset("assets/tptp.png"),
                      title: Text(
                        'ADD PLAN',
                        style: TextStyle(
                            fontSize: 22.0, fontWeight: FontWeight.w600),
                      ),
                      description: Text(
                        "Apakah anda yakin ingin membuat rencana hari ini ? ",
                        textAlign: TextAlign.center,
                        style: TextStyle(),
                      ),
                      entryAnimation: EntryAnimation.RIGHT_LEFT,
                      buttonCancelText: Text("Cek kembali !",
                          style: TextStyle(color: Colors.white)),
                      buttonOkText: Text("Kirim Rencana",
                          style: TextStyle(color: Colors.white)),
                      onOkButtonPressed: () {
                        addData(
                            id_user,
                            nik,
                            tgl_mulai,
                            tgl_akhir,
                            area,
                            subArea,
                            status,
                            statusCuti,
                            opsiapproval,
                            imageFile,
                            context);

                        Navigator.pop(context, true);
                      },
                    ));
            opsiapproval = "$_mySelection,$id_kabag";
            if (_mySelection == null) {
              opsiapproval = ",$id_kabag";
            }
            if (id_kabag == null) {
              opsiapproval = "$_mySelection,";
            }
            print(id_user);
            print(nik);
            print(tgl_mulai);
            print(tgl_akhir);
            print(area);
            print(subArea);
            print(status);
            print(statusCuti);
            print(keterangan.text);
            print(opsiapproval);
          },
        ),
        Padding(
          padding: EdgeInsets.only(top: 10.0),
        ),
      ],
    );
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Image.asset(
              "assets/tptp.png",
              width: 70,
              height: 70,
            ),
            onPressed: () {},
          )
        ],
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
            // Navigator.pushReplacement(
            //     context,
            //     MaterialPageRoute(
            //         builder: (BuildContext context) => HomePage()));
          },
        ),
        title: Text(
          'TAMBAH RENCANA ',
          style: TextStyle(color: Colors.black87),
        ),
        elevation: 4,
        backgroundColor: Colors.white,
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 5.0, right: 5.0),
              child: Card(
                elevation: 6.0,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    children: <Widget>[
                      Animator(
                        duration: Duration(seconds: 2),
                        builder: (anim) => Center(
                          child: Transform.scale(
                            scale: anim.value,
                            child: Column(
                              children: <Widget>[
                                Center(
                                  child: Image.asset(
                                    "assets/logo.png",
                                    width: 50,
                                    height: 50,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 10.0),
                                ),
                                Center(
                                  child: Text(
                                    "Form Rencana PTPN IX",
                                    style: TextStyle(fontSize: 25.0),
                                  ),
                                ),
                                Center(
                                  child: Text(
                                    "HR-MANAGEMENT SYSTEM",
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontStyle: FontStyle.italic),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SingleChildScrollView(
                        child: form,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _openGalerry(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      imageFile = picture;
    });
    Navigator.of(context).pop();
  }

  _openCamera(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      imageFile = picture;
    });
    Navigator.of(context).pop();
  }

  Future<void> _showChoiceDialog(BuildContext context) {
    print("masuk");
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Pilih Gambar"),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  GestureDetector(
                    child: Text("Galery"),
                    onTap: () {
                      _openGalerry(context);
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  GestureDetector(
                    child: Text("Camera"),
                    onTap: () {
                      _openCamera(context);
                    },
                  )
                ],
              ),
            ),
          );
        });
  }

  void pilihOpsi(String value) {
    setState(() {
      _opsi = value;
    });
  }
}
