import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EvidenceGambar extends StatefulWidget {
  final String url;

  const EvidenceGambar({Key key, this.url}) : super(key: key);
  @override
  _EvidenceGambarState createState() => _EvidenceGambarState();
}

class _EvidenceGambarState extends State<EvidenceGambar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Detail Evidence Gambar"),),
      body: Center(child:Image.network(widget.url),),
    );
    
  }
}