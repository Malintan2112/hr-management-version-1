import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as chart;
import 'package:hr_management_deploy/model/kegiatanmodel.dart';

class GrafikPage extends StatefulWidget {
  @override
  _GrafikPageState createState() => _GrafikPageState();
}

class Sales {
  final String hari;
  final int terjual;
  final chart.Color color;

  Sales(this.hari, this.terjual, Color color)
      : this.color = chart.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}

class _GrafikPageState extends State<GrafikPage> {
  List<KM> users;
  List<KM> selectedUser;
  bool sort;
  @override
  void initState() {
    sort = false;
    selectedUser = [];
    users = KM.getKM();
    // TODO: implement initState
    super.initState();
  }

  onSortColumn(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        users.sort((a, b) => a.tglAwal.compareTo(b.tglAwal));
      } else {
        users.sort((a, b) => b.tglAwal.compareTo(a.tglAwal));
      }
    }
  }

  List<DataColumn> columnLog() {
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      return [
        DataColumn(
            label: Text("Tgl Awal"),
            numeric: false,
            tooltip: "this is nama awal ",
            onSort: (columnIndex, ascending) {
              setState(() {
                sort = !sort;
              });
              onSortColumn(columnIndex, ascending);
            }),
        DataColumn(
            label: Text("Akhir"),
            numeric: false,
            tooltip: "this is nama akhir "),
        DataColumn(
            label: Text("Opsi"),
            numeric: false,
            tooltip: "this is nama akhir "),
        // DataColumn(
        //     label: Text("Status"),
        //     numeric: false,
        //     tooltip: "this is nama akhir ")
      ];
    } else {
      return [
        DataColumn(
            label: Text("Tgl Awal"),
            numeric: false,
            tooltip: "this is nama awal ",
            onSort: (columnIndex, ascending) {
              setState(() {
                sort = !sort;
              });
              onSortColumn(columnIndex, ascending);
            }),
        DataColumn(
            label: Text("Akhir"),
            numeric: false,
            tooltip: "this is nama akhir "),
        DataColumn(
            label: Text("Opsi"),
            numeric: false,
            tooltip: "this is nama akhir "),
        // DataColumn(
        //     label: Text("Keterangan"),
        //     numeric: false,
        //     tooltip: "this is nama akhir "),
        DataColumn(
            label: Text("Status"),
            numeric: false,
            tooltip: "this is nama akhir "),
      ];
    }
  }

  List<DataCell> rowLog(KM user) {
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      return [
        DataCell(Text(user.tglAwal), onTap: () {}),
        DataCell(Text(user.tglAkhir)),
        DataCell(Text(user.opsi)),
        // DataCell(Text(user.kt)),
        // DataCell(Text(user.status)),
      ];
    } else {
      return [
        DataCell(Text(user.tglAwal), onTap: () {}),
        DataCell(Text(user.tglAkhir)),
        DataCell(Text(user.opsi)),
        // DataCell(Text(user.kt)),
        DataCell(Text(user.status,style: TextStyle(color: Colors.green),)),
      ];
    }
  }

  DataTable dataBody() {
    return DataTable(
      sortAscending: sort,
      sortColumnIndex: 0,
      columns: columnLog(),
      rows: users
          .map(
            (user) => DataRow(cells: rowLog(user)),
          )
          .toList(),
    );
  }

  void deleteSelected() async {
    setState(() {
      if (selectedUser.isNotEmpty) {
        List<KM> temp = [];
        temp.addAll(selectedUser);
        for (KM user in temp) {
          users.remove(user);
          selectedUser.remove(user);
        }
      } else {}
    });
  }

  @override
  Widget build(BuildContext context) {
    var data = [
      Sales("Hadir", 9, Colors.green),
      Sales("Terlambat", 2, Colors.orange),
      Sales("Sakit", 1, Colors.purple),
      Sales("Alpha", 0, Colors.redAccent),
      Sales("Cuti", 1, Colors.blueGrey),
      Sales("Dinas", 1, Colors.lightBlue),
      Sales("Dispensasi", 1, Colors.lime),
    ];
    var series = [
      chart.Series(
          domainFn: (Sales sales, _) => sales.hari,
          measureFn: (Sales sales, _) => sales.terjual,
          id: "Sales",
          data: data,
          labelAccessorFn: (Sales sales, _) => '${sales.terjual.toString()}',
          colorFn: (Sales sales, _) => sales.color),
    ];
    var grafik = chart.BarChart(
      series,
      // vertical: true,s
      barRendererDecorator: chart.BarLabelDecorator<String>(),
      // domainAxis: chart.OrdinalAxisSpec(renderSpec: chart.NoneRenderSpec()),
    );

    return Scaffold(
      appBar: AppBar(
        elevation: 3,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "GRAFIK PERFORMA",
          style: TextStyle(color: Colors.black87),
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          IconButton(
            icon: Image.asset(
              "assets/graph.png",
              width: 70,
              height: 70,
            ),
            onPressed: () {
              // logOut();
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (BuildContext context) => LoginPage()));
            },
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            Card(
                elevation: 3.0,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 10.0,
                    ),
                    Animator(
                      duration: Duration(seconds: 2),
                      builder: (anim) => Center(
                        child: Transform.scale(
                          scale: anim.value,
                          child: Image.asset(
                            'assets/logo.png',
                            width: 40,
                            height: 40,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Animator(
                      duration: Duration(seconds: 2),
                      builder: (anim) => Center(
                        child: Transform.scale(
                          scale: anim.value,
                          child: Center(
                            child: Text("GRAFIK ABSENSI 15 HARI KERJA"),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SizedBox(
                        child: grafik,
                        height: 200.0,
                      ),
                    )
                  ],
                )),
            Card(
              elevation: 3.0,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Text("Daftar Kegiatan"),
                    Center(
                      child: SizedBox(
                        width: 500,
                        child: dataBody(),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
