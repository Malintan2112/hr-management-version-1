import 'dart:convert';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:hr_management_deploy/bloc/daftar_rencana-bloc.dart';
import 'package:hr_management_deploy/page/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DPPage extends StatefulWidget {
  @override
  _DPPageState createState() => _DPPageState();
}

class _DPPageState extends State<DPPage> {
  String idToken = "";
  getApiToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idToken = preferences.getString("idToken");
      print("id token $idToken");
    });
  }

  savePesanDP() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("pesandp", 0);
      preferences.commit();
    });
  }

  DaftarRencanaBlock drb;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    savePesanDP();
    getApiToken();
    // getData();
    valve = false;
  }

  bool valve;
  @override
  Widget build(BuildContext context) {
    drb = Provider.of<DaftarRencanaBlock>(context, listen: true);
    if (valve == false) {
      // drb.idToken = idToken;
      drb.fetchDaftarRencana();
      valve = true;
    }
    Widget circular() {
      // drb.fetchDaftarRencana();
      drb.fetchDaftarRencana();

      return Center(
        child: CircularProgressIndicator(),
      );
    }
GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
    Widget list() {
      return ListView.builder(
        itemCount: drb.daftarRencana.length,
        itemBuilder: (BuildContext context, int index) {
          // return Text(drb.daftarRencana[index].tangal_akhir);
          var status;
          var approvalstatus;
          Color color;
          switch (drb.daftarRencana[index].status) {
            case 1:
              status = "CUTI";
              break;
            case 2:
              status = "SAKIT";
              break;
            case 3:
              status = "DINAS";
              break;
            case 4:
              status = "MEMO TERLAMBAT";
              break;
            case 5:
              status = "DISPENSASI";
              break;
            case 6:
              status = "LEMBUR";
              break;
            default:
          }
          switch (drb.daftarRencana[index].approved_status) {
            case 1:
              approvalstatus = "Approved";
              color = Colors.green;
              break;
            case 2:
              approvalstatus = "Semi Aprroved ";
              color = Colors.blue;

              break;
            case 0:
              approvalstatus = "Rejected ";
              color = Colors.red;
              break;

            default:
            approvalstatus = "Waiting ...";
              color = Colors.orange;
          }
          return new Container(
            padding: const EdgeInsets.all(1.0),
            child: new GestureDetector(
              onTap: () {},
              child: new Card(
                child: new ListTile(
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                "${status}",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                ' ( ${drb.daftarRencana[index].tanggal_awal} )',
                                textAlign: TextAlign.right,
                              ),
                            ],
                          )
                        ],
                      ),
                      Divider(),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                                "Keterangan : \n${drb.daftarRencana[index].desc_keterangan}"),
                          ),
                          (approvalstatus == "Waiting ...")
                              ? GestureDetector(
                                  onTap: () {
                                    showDialog(
                                      
                                context: context,
                                builder: (_) => AssetGiffyDialog(
                                  
                                      image: Image.asset("assets/refuse.png"),
                                      buttonCancelText: Text("Tidak",style: TextStyle(color: Colors.white),),
                                      buttonOkColor: Colors.red,
                                      buttonOkText: Text("Iya ,cancel rencana",style: TextStyle(color: Colors.white)),
                                      title: Text(
                                        'CANCEL PLAN',
                                        style: TextStyle(
                                            fontSize: 22.0,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      description: Text(
                                        'Apakah anda yakin  ingin membatalkan rencana ?',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(),
                                      ),
                                      entryAnimation: EntryAnimation.RIGHT_LEFT,
                                      onOkButtonPressed: () {
                                        drb.deletRencana(drb.daftarRencana[index].id_ma, context, _scaffoldKey);
                                      },
                                    ));
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 40),
                                    child: Container(
                                      width: 30,
                                      height: 30,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.red),
                                      child: Center(
                                        child: Icon(
                                          Icons.cancel,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              : Offstage()
                        ],
                      )
                    ],
                  ),
                  leading: CircleAvatar(
                    backgroundImage: AssetImage('assets/dp.png'),
                  ),
                  // subtitle: new Text("QTY : ${list[i]['qty']}"),
                  subtitle: Text(
                    "${approvalstatus}",
                    style: TextStyle(color: color),
                  ),
                ),
              ),
            ),
          );
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Image.asset(
              "assets/dp.png",
              width: 70,
              height: 70,
            ),
            onPressed: () {
              // logOut();
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (BuildContext context) => LoginPage()));
            },
          )
        ],
        title: Text('DAFTAR RENCANA'),
        elevation: 0,
        backgroundColor: Color(0xFFf45d40),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            savePesanDP();
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => HomePage()));
          },
        ),
      ),
      body: drb.daftarRencana != null ? list() : circular(),
    );
  }
}
