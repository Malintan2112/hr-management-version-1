import 'dart:convert';
import 'dart:io';
import 'package:hr_management_deploy/bloc/approve_rencana_bloc.dart';
import 'package:hr_management_deploy/bloc/user-bloc.dart';
import 'package:hr_management_deploy/page/content/evidence_gambar.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../home.dart';
import 'evidence_pdf.dart';
import 'package:giffy_dialog/giffy_dialog.dart';

class ApprovePage extends StatefulWidget {
  @override
  _ApprovePageState createState() => _ApprovePageState();
}

class _ApprovePageState extends State<ApprovePage> {
  String idToken = "";
  getApiToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idToken = preferences.getString("idToken");
      print("id token $idToken");
    });
  }

  savePesanAPP() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("pesanapp", 0);
      preferences.commit();
    });
  }

  ApproveRencanaBlock arb;
  UserBlock userBlock;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    savePesanAPP();
    getApiToken();
    valve = false;
    userBlock = Provider.of<UserBlock>(context, listen: false);
  }

  bool valve;
  String urlPDFPath;
  Future<File> getFileFromUrl(String url) async {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Mohon tunggu ....."),
    ));
    try {
      var data = await http.get(url);
      var bytes = data.bodyBytes;
      var dir = await getApplicationDocumentsDirectory();
      File file = File("${dir.path}/mypdfonline.pdf");
      File urlFile = await file.writeAsBytes(bytes);

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PDFViewPage(
                    path: urlFile.path,
                  )));
    } catch (e) {
      throw Exception("Error opening url file");
    }
  }

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    arb = Provider.of<ApproveRencanaBlock>(context, listen: true);

    if (valve == false) {
      arb.fetchApproveRencana();
      valve = true;
    }
    Widget circular() {
      // drb.fetchDaftarRencana();
      arb.fetchApproveRencana();

      return Center(
        child: CircularProgressIndicator(),
      );
    }

    GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

    Widget list() {
      return ListView.builder(
        itemCount: arb.approveRencana.length,
        itemBuilder: (BuildContext context, int index) {
          // return Text(drb.daftarRencana[index].tangal_akhir);
          var status;
          var approvalstatus;
          Color color;
          Widget evidence = Offstage();
          switch (arb.approveRencana[index].status) {
            case 1:
              status = "CUTI";
              break;
            case 2:
              status = "SAKIT";
              break;
            case 3:
              status = "DINAS";
              break;
            case 4:
              status = "MEMO TERLAMBAT";
              break;
            case 5:
              status = "DISPENSASI";
              break;
            case 6:
              status = "LEMBUR";
              break;
            default:
          }
          switch (arb.approveRencana[index].approved_status) {
            case 1:
              approvalstatus = "Approved";
              color = Colors.green;
              break;
            case 2:
              approvalstatus = "Semi Aprroved ";
              color = Colors.blue;

              break;
            case 3:
              approvalstatus = "Waiting ...";
              color = Colors.yellow;
              break;

            default:
          }
          return new Container(
            padding: const EdgeInsets.all(1.0),
            child: new GestureDetector(
              onTap: () {},
              child: new Card(
                child: new ListTile(
                    title: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(
                              "${arb.approveRencana[index].name}",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              "${status}",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              ' ( ${arb.approveRencana[index].tanggal_awal})',
                              textAlign: TextAlign.right,
                            )
                          ],
                        ),
                        Divider(),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                  "Keterangan : \n${arb.approveRencana[index].desc_keterangan}"),
                            ),
                            (arb.approveRencana[index].evidence != "")
                                ? GestureDetector(
                                    child: Container(
                                      height: 20,
                                      width:
                                          MediaQuery.of(context).size.width / 6,
                                      decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                            colors: [
                                              Color(0xFF0097A7),
                                              Color(0xFF26CC6DA)
                                            ],
                                          ),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(50))),
                                      child: Center(
                                        child: Text(
                                          'evidence'.toUpperCase(),
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    onTap: () {
                                      FocusScope.of(context)
                                          .requestFocus(FocusNode());
                                      // check();
                                      switch (
                                          arb.approveRencana[index].ektension) {
                                        case "pdf":
                                          getFileFromUrl(
                                                  "http://dash.ptpnix.co.id/hrsistems/public/evidence/${arb.approveRencana[index].evidence}")
                                              .then((f) {
                                            setState(() {
                                              urlPDFPath = f.path;
                                              print(urlPDFPath);
                                            });
                                          });
                                          break;
                                        case "jpg":
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (BuildContext) =>
                                                      EvidenceGambar(
                                                        url:
                                                            "http://dash.ptpnix.co.id/hrsistems/public/evidence/${arb.approveRencana[index].evidence}",
                                                      )));
                                          break;
                                        default:
                                      }
                                    },
                                  )
                                : Offstage()
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                    leading: CircleAvatar(
                      backgroundImage: NetworkImage((arb
                                  .approveRencana[index].file_url !=
                              null)
                          ? "http://dash.ptpnix.co.id/hrsistems/public/profil/${arb.approveRencana[index].file_url}"
                          : "https://www.shareicon.net/data/2016/09/01/822739_user_512x512.png"),
                    ),
                    // subtitle: new Text("QTY : ${list[i]['qty']}"),
                    subtitle: Row(
                      children: <Widget>[
                        GestureDetector(
                          child: Container(
                            height: 40,
                            width: MediaQuery.of(context).size.width / 4,
                            decoration: BoxDecoration(
                                color: Colors.redAccent,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50))),
                            child: Center(
                              child: Text(
                                'reject'.toUpperCase(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (_) => AssetGiffyDialog(
                                      buttonOkColor: Colors.red,
                                      buttonOkText: Text("Reject Plan",
                                          style:
                                              TextStyle(color: Colors.white)),
                                      image: Image.asset("assets/refuse.png"),
                                      title: Text(
                                        'REFUSE',
                                        style: TextStyle(
                                            fontSize: 22.0,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      description: Text(
                                        'Apakah anda yakin  menolak ${arb.approveRencana[index].name} untuk rencana ${status} nya?',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(),
                                      ),
                                      entryAnimation: EntryAnimation.RIGHT_LEFT,
                                      onOkButtonPressed: () {
                                        arb.cancelRencana(
                                            arb.approveRencana[index].id_ma,
                                            context,
                                            _scaffoldKey);
                                      },
                                    ));
                          },
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        GestureDetector(
                          child: Container(
                            height: 40,
                            width: MediaQuery.of(context).size.width / 4,
                            decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50))),
                            child: Center(
                              child: Text(
                                'Approve'.toUpperCase(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (_) => AssetGiffyDialog(
                                      buttonOkText: Text("Approve Plan",
                                          style:
                                              TextStyle(color: Colors.white)),
                                      image: Image.asset(
                                        "assets/ttd.png",
                                        height: 50,
                                        width: 50,
                                      ),
                                      title: Text(
                                        'APPROVE',
                                        style: TextStyle(
                                            fontSize: 22.0,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      description: Text(
                                        'Apakah anda yakin mempersetujui ${arb.approveRencana[index].name} untuk rencana ${status} nya?',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(),
                                      ),
                                      entryAnimation: EntryAnimation.RIGHT_LEFT,
                                      onOkButtonPressed: () {
                                        arb.approveRencanaMA(
                                            arb.approveRencana[index].id_ma,userBlock.detailuser.id_jabatan,
                                            context,
                                            _scaffoldKey);
                                      },
                                    ));
                          },
                        ),
                      ],
                    )),
              ),
            ),
          );
        },
      );
    }

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Image.asset(
              "assets/ttd.png",
              width: 70,
              height: 70,
            ),
            onPressed: () {
              // logOut();
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (BuildContext context) => LoginPage()));
            },
          )
        ],
        title: Text('APPROVE RENCANA '),
        elevation: 0,
        backgroundColor: Color(0xFFf45d40),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            // savePesanaP();
            savePesanAPP();
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => HomePage()));
          },
        ),
      ),
      body: arb.approveRencana != null ? list() : circular(),
    );
  }
}

class ItemList extends StatelessWidget {
  final List list;
  ItemList({this.list});
  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemCount: list == null ? 0 : list.length,
      itemBuilder: (context, i) {
        var status;
        var approvalstatus;
        Color color;
        switch (list[i]['status']) {
          case 1:
            status = "CUTI";
            break;
          case 2:
            status = "SAKIT";
            break;
          case 3:
            status = "DINAS";
            break;
          case 4:
            status = "MEMO TERLAMBAT";
            break;
          case 5:
            status = "DISPENSASI";
            break;
          case 6:
            status = "LEMBUR";
            break;
          default:
        }
        switch (list[i]['approved_status']) {
          case 1:
            approvalstatus = "Approved";
            color = Colors.green;
            break;
          case 2:
            approvalstatus = "Semi Aprroved ";
            color = Colors.blue;

            break;
          case 3:
            approvalstatus = "Waiting ...";
            color = Colors.yellow;
            break;

          default:
        }
        return new Container(
          padding: const EdgeInsets.all(1.0),
          child: new GestureDetector(
            onTap: () {},
            child: new Card(
              child: new ListTile(
                // title: new Text(list[i]['nama']+"masuk"),
                title: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "${status}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          ' ( ${list[i]['tanggal_awal']})',
                          textAlign: TextAlign.right,
                        )
                      ],
                    ),
                    Divider(),
                    Text("Keterangan : \n${list[i]['desc_keterangan']}")
                  ],
                ),
                leading: CircleAvatar(
                  backgroundImage: AssetImage('assets/ttd.png'),
                ),
                // subtitle: new Text("QTY : ${list[i]['qty']}"),
                subtitle: Text(
                  "${approvalstatus}",
                  style: TextStyle(color: color),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
