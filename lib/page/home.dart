import 'dart:io';

import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/material.dart';
import 'package:hr_management_deploy/bloc/user-bloc.dart';
import 'package:hr_management_deploy/page/menu.dart';
import 'package:hr_management_deploy/page/profile.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'login.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  UserBlock userBlock;
  void initState() {
    // TODO: implement initState
    super.initState();
    // userBlock.getDetail(idToken);

  }

  int _currentIndex = 0;
  final List<Widget> _child = [MenuPage(), ProfilPage()];
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    userBlock = Provider.of<UserBlock>(context, listen: true);

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Image.asset("assets/logo.png"),
            onPressed: () {
            
            },
          )
        ],
        title: Text('HR-MANAGEMENT SISTEM '),
        elevation: 0,
        backgroundColor: Color(0xFFf45d40),
        leading: IconButton(
          icon: Image.asset(
            "assets/icon.png",
            width: 70,
            height: 70,
          ),
          onPressed: () {},
        ),
      ),
      body: _child[_currentIndex],
      bottomNavigationBar: FancyBottomNavigation(
        inactiveIconColor: Color(0xFFf5851f),
        circleColor: Color(0xFFf5851f),
        tabs: [
          TabData(iconData: Icons.home, title: 'Menu'),
          TabData(iconData: Icons.supervised_user_circle, title: 'Profil'),
        ],
        onTabChangedListener: (int position) {
          setState(() {
            _currentIndex = position;
          });
        },
      ),
    );
  }

   logOut() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    // userBlock.detailuser.remo
    setState(() {
      var value = preferences.setInt("value", null);
      preferences.setString("idToken", null);
    });
    // exit(0);
  }
}
