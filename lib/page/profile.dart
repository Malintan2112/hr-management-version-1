import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:hr_management_deploy/bloc/user-bloc.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login.dart';

class ProfilPage extends StatefulWidget {
  @override
  _ProfilPageState createState() => _ProfilPageState();
}

class _ProfilPageState extends State<ProfilPage> {
  UserBlock userBlock;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userBlock = Provider.of<UserBlock>(context, listen: false);
  }

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  logOut() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    // userBlock.detailuser.remo
    _firebaseMessaging
        .unsubscribeFromTopic(userBlock.detailuser.id_user.toString());

    setState(() {
      var value = preferences.setInt("value", null);
      preferences.setString("idToken", null);
    });
    // exit(0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
          Card(
            elevation: 5.0,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Center(
                    child: Container(
                      width: 120,
                      height: 120,
                      child: Stack(
                        children: <Widget>[
                          Container(
                            width: 120,
                            height: 120,
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black45),
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: NetworkImage((userBlock
                                              .detailuser.file_url !=
                                          null)
                                      ? "http://dash.ptpnix.co.id/hrsistems/public/profil/${userBlock.detailuser.file_url}"
                                      : "https://www.shareicon.net/data/2016/09/01/822739_user_512x512.png"),
                                  fit: BoxFit.fill,
                                )),
                            child: Column(),
                          ),
                          // Padding(
                          //   padding: const EdgeInsets.only(top: 90.0, left: 80),
                          //   child: Container(
                          //     width: 30,
                          //     height: 30,
                          //     decoration: BoxDecoration(
                          //         shape: BoxShape.circle, color: Colors.orange),
                          //     child: Center(
                          //       child: Icon(
                          //         Icons.edit,
                          //         color: Colors.white,
                          //       ),
                          //     ),
                          //   ),
                          // )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "IDENTITAS DIRI",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
                SizedBox(
                  child: Divider(),
                  width: 100,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 50.0, top: 20.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "NAMA              : ${userBlock.detailuser.nama_pegawai}",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "BAGIAN           : ${userBlock.detailuser.nama_bagian}",
                            style: TextStyle(fontSize: 15),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "JABATAN        : ${userBlock.detailuser.nama_jabatan}",
                            style: TextStyle(fontSize: 15),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "NIK                   : ${userBlock.detailuser.nik}",
                            style: TextStyle(fontSize: 15),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "GOLONGAN    : ${userBlock.detailuser.golongan}",
                            style: TextStyle(fontSize: 15),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                // GestureDetector(
                //   child: Container(
                //     height: 45,
                //     width: MediaQuery.of(context).size.width / 1.5,
                //     decoration: BoxDecoration(
                //         border: Border.all(color: Colors.black38),
                //         borderRadius: BorderRadius.all(Radius.circular(50))),
                //     child: Center(
                //       child: Text(
                //         'UBAH PASSWORD'.toUpperCase(),
                //         style: TextStyle(
                //             color: Colors.black54, fontWeight: FontWeight.bold),
                //       ),
                //     ),
                //   ),
                //   onTap: () {
                //     showModalBottomSheet(
                //         isScrollControlled: true,
                //         context: context,
                //         builder: (context) {
                //           return Container(
                //             height: MediaQuery.of(context).size.height / 1.2,
                //             color: Color(0xFF737373),
                //             child: Container(
                //               decoration: BoxDecoration(
                //                   color: Theme.of(context).canvasColor,
                //                   borderRadius: BorderRadius.only(
                //                       topLeft: const Radius.circular(10),
                //                       topRight: const Radius.circular(10))),
                //               child: Column(
                //                 children: <Widget>[
                //                   SizedBox(
                //                     height: 20,
                //                   ),
                //                   Text(
                //                     "FORM UPDATE PASSWORD",
                //                     style: TextStyle(
                //                         fontWeight: FontWeight.bold,
                //                         fontSize: 18.0),
                //                   ),
                //                   Container(
                //                     width:
                //                         MediaQuery.of(context).size.width / 1.2,
                //                     height: 45,
                //                     margin: EdgeInsets.only(top: 32),
                //                     padding: EdgeInsets.only(
                //                         top: 4, left: 16, right: 16, bottom: 4),
                //                     decoration: BoxDecoration(
                //                         borderRadius: BorderRadius.all(
                //                             Radius.circular(50)),
                //                         color: Colors.white,
                //                         boxShadow: [
                //                           BoxShadow(
                //                               color: Colors.black12,
                //                               blurRadius: 5)
                //                         ]),
                //                     child: TextFormField(
                //                       decoration: InputDecoration(
                //                         border: InputBorder.none,
                //                         icon: Icon(
                //                           Icons.vpn_key,
                //                           color: Colors.grey,
                //                         ),
                //                         hintText: 'Password Lama',
                //                       ),
                //                       obscureText: true,
                //                     ),
                //                   ),
                //                   Container(
                //                     width:
                //                         MediaQuery.of(context).size.width / 1.2,
                //                     height: 45,
                //                     margin: EdgeInsets.only(top: 32),
                //                     padding: EdgeInsets.only(
                //                         top: 4, left: 16, right: 16, bottom: 4),
                //                     decoration: BoxDecoration(
                //                         borderRadius: BorderRadius.all(
                //                             Radius.circular(50)),
                //                         color: Colors.white,
                //                         boxShadow: [
                //                           BoxShadow(
                //                               color: Colors.blue, blurRadius: 5)
                //                         ]),
                //                     child: TextFormField(
                //                       // onSaved: (e) => password = e,
                //                       decoration: InputDecoration(
                //                         border: InputBorder.none,
                //                         icon: Icon(Icons.vpn_key,
                //                             color: Colors.blue),
                //                         hintText: 'Password Baru',
                //                         // suffixIcon: IconButton(
                //                         //   onPressed: () {
                //                         //     // showHide();
                //                         //   },
                //                         //   // icon: Icon(secureText
                //                         //   //     ? Icons.visibility_off
                //                         //   //     : Icons.visibility),
                //                         // ),
                //                       ),
                //                       obscureText: true,
                //                     ),
                //                   ),
                //                   Container(
                //                     width:
                //                         MediaQuery.of(context).size.width / 1.2,
                //                     height: 45,
                //                     margin: EdgeInsets.only(top: 32),
                //                     padding: EdgeInsets.only(
                //                         top: 4, left: 16, right: 16, bottom: 4),
                //                     decoration: BoxDecoration(
                //                         borderRadius: BorderRadius.all(
                //                             Radius.circular(50)),
                //                         color: Colors.white,
                //                         boxShadow: [
                //                           BoxShadow(
                //                               color: Colors.blue, blurRadius: 5)
                //                         ]),
                //                     child: TextFormField(
                //                       // onSaved: (e) => password = e,
                //                       decoration: InputDecoration(
                //                         border: InputBorder.none,
                //                         icon: Icon(
                //                           Icons.vpn_key,
                //                           color: Colors.blue,
                //                         ),
                //                         hintText: 'Repassword Baru',
                //                         // suffixIcon: IconButton(
                //                         //   onPressed: () {
                //                         //     // showHide();
                //                         //   },
                //                         //   // icon: Icon(secureText
                //                         //   //     ? Icons.visibility_off
                //                         //   //     : Icons.visibility),
                //                         // ),
                //                       ),
                //                       obscureText: true,
                //                     ),
                //                   ),
                //                   SizedBox(
                //                     height: 30,
                //                   ),
                //                   GestureDetector(
                //                     child: Container(
                //                       height: 40,
                //                       width:
                //                           MediaQuery.of(context).size.width / 2,
                //                       decoration: BoxDecoration(
                //                           color: Colors.green,
                //                           borderRadius: BorderRadius.all(
                //                               Radius.circular(50))),
                //                       child: Center(
                //                         child: Text(
                //                           'Simpan'.toUpperCase(),
                //                           style: TextStyle(
                //                               color: Colors.white,
                //                               fontWeight: FontWeight.bold),
                //                         ),
                //                       ),
                //                     ),
                //                     onTap: () {
                                      
                //                     },
                //                   ),
                //                 ],
                //               ),
                //             ),
                //           );
                //         });
                //     FocusScope.of(context).requestFocus(FocusNode());
                //     // check();
                //   },
                // ),
                SizedBox(
                  height:  MediaQuery.of(context).size.height / 7,
                ),
                GestureDetector(
                  child: Container(
                    height: 45,
                    width: MediaQuery.of(context).size.width / 1.5,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [Color(0xFFf45d27), Color(0xFFf5851f)],
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(50))),
                    child: Center(
                      child: Text(
                        'Logout'.toUpperCase(),
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  onTap: () {
                    logOut();
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => LoginPage()));
                    // check();
                  },
                ),
             
              ],
            ),
        
      ),
    );
  }
}
