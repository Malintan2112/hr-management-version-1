import 'dart:convert';
import 'package:animator/animator.dart';
import 'package:hr_management_deploy/bloc/login-bloc.dart';
import 'package:hr_management_deploy/bloc/user-bloc.dart';
import 'package:hr_management_deploy/page/content/welcome.dart';
// import 'package:hr_management_deploy/constant/constant.dart';
import 'package:hr_management_deploy/page/home.dart';
import 'package:hr_management_deploy/page/menu.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

// membuat kondisi memastikan login
enum LoginStatus { notSignIn, signIn }

class _LoginPageState extends State<LoginPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final login_bloc = LoginBloc();
  LoginStatus _loginStatus = LoginStatus.notSignIn;
  String username, password;
  bool secureText = true;
  final _key = GlobalKey<FormState>();
  // fungsi menampilkan dan menghiden password
  showHide() {
    setState(() {
      secureText = !secureText;
      print(secureText);
    });
  }

  savePrev(int value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("value", value);
      preferences.commit();
    });
  }

  idToken(String value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setString("idToken", value);
      preferences.commit();
    });
  }

  idUser(String value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setString("idUser", value);
      preferences.commit();
    });
  }

  login() async {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Mohon tunggu ....."),
      action: SnackBarAction(
        label: 'Cancel',
        textColor: Colors.blue,
        onPressed: () {
          return null;
        },
      ),
    ));
    // var url = "http://10.0.2.2/android/login.php";
    // final response = await http
    //     .post(url, body: {'username': username, 'password': password});
    final response = await http.get(
        "http://dash.ptpnix.co.id/hrsistems/api/loginuser?username=${username}&password=${password}!");
    var datauser = jsonDecode(response.body);
    if (response != 200) {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Row(
            children: <Widget>[
              Text(
                "Terjadi kesalahan .....",
                style: TextStyle(color: Colors.yellow),
              ),
              Spacer(),
              Icon(
                Icons.warning,
                color: Colors.yellow,
              )
            ],
          ),
        ),
      );
    } else if (datauser.error == "invalid_credentials") {
      print('masuk');
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Row(
            children: <Widget>[
              Text(
                "kombinasi salah ",
                style: TextStyle(color: Colors.yellow),
              ),
              Spacer(),
              Icon(
                Icons.lock,
                color: Colors.yellow,
              )
            ],
          ),
        ),
      );
      return null;
    }
    if (datauser.length == 0) {
    } else {
      print(datauser[0]['api_token']);
      setState(() {
        // menyimpan umpan balik pada share preference
        _loginStatus = LoginStatus.signIn;
        savePrev(datauser.length);
        idToken(datauser[0]['api_token']);
        idUser(datauser[0]['username']);
      });
    }
  }

// fungsi mengecek kondisi form login
  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      login();
    }
  }

  saveStatusMenu() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setBool("status_menu", false);
      preferences.commit();
    });
  }

  var value;
// fungsi mengecek apakah sudah login atau belum
  getPrev() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      value = preferences.getInt("value");
      _loginStatus = value == 1 ? LoginStatus.signIn : LoginStatus.notSignIn;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPrev();
    saveStatusMenu();
  }

  @override
  Widget build(BuildContext context) {
    final UserBlock userBlock = Provider.of<UserBlock>(context);
    switch (_loginStatus) {
      case LoginStatus.notSignIn:
        return Scaffold(
          key: _scaffoldKey,
          body: Form(
            key: _key,
            child: Padding(
              padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
              child: Container(
                  child: ListView(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: (MediaQuery.of(context).orientation ==
                            Orientation.portrait)
                        ? MediaQuery.of(context).size.height / 4
                        : MediaQuery.of(context).size.height / 2.5,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [Color(0xFFf45d40), Color(0xFFf5851f)],
                        ),
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(90),
                            bottomLeft: Radius.circular(90))),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 10, right: 32),
                              child: Text(
                                'HR-MANAGEMENT SISTEM V.1.0',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w300,
                                    fontSize: 10),
                              ),
                            ),
                          ),
                          Spacer(),
                          Animator(
                            duration: Duration(seconds: 2),
                            builder: (anim) => Center(
                              child: Transform.scale(
                                scale: anim.value,
                                child: Column(
                                  children: <Widget>[
                                    Align(
                                        alignment: Alignment.center,
                                        child: Image.asset(
                                          "assets/logo.png",
                                          width: 70,
                                          height: 70,
                                        )),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Center(
                                        child: Text(
                                          "PTPN9",
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                  ),
                  Animator(
                    repeats: 1,
                    duration: Duration(seconds: 2),
                    builder: (anim) => Opacity(
                      opacity: anim.value,
                      child: Column(
                        children: <Widget>[
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 20.0),
                              child: Align(
                                child: Text(
                                  ' LOGIN ',
                                  style: TextStyle(
                                      color: Colors.black26,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: (MediaQuery.of(context).orientation ==
                            Orientation.portrait)
                        ? MediaQuery.of(context).size.height / 2
                        : MediaQuery.of(context).size.height / 1.5,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(top: 20),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          height: 45,
                          padding: EdgeInsets.only(
                              top: 4, left: 16, right: 16, bottom: 4),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(50)),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(color: Colors.black12, blurRadius: 5)
                              ]),
                          child: StreamBuilder<String>(
                            stream: login_bloc.nik,
                            builder: (context, snapshot) => TextFormField(
                              // autofocus: true,
                              keyboardType: TextInputType.number,
                              onChanged: (s) => login_bloc.nikChanged.add(s),
                              validator: (e) {},
                              onSaved: (e) {
                                username = e;
                              },
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  icon: Icon(
                                    Icons.supervised_user_circle,
                                    color: Colors.grey,
                                  ),
                                  hintText: 'Nomor Induk Karyawan',
                                  suffixIcon: (snapshot.error != null)
                                      ? Animator(
                                          repeats: 1,
                                          duration: Duration(seconds: 2),
                                          builder: (anim) => Opacity(
                                            opacity: anim.value,
                                            child: Icon(
                                              Icons.warning,
                                              color: Colors.red,
                                            ),
                                          ),
                                        )
                                      : Icon(
                                          Icons.check,
                                          color: Colors.blueAccent,
                                        )),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          height: 45,
                          margin: EdgeInsets.only(top: 32),
                          padding: EdgeInsets.only(
                              top: 4, left: 16, right: 16, bottom: 4),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(50)),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(color: Colors.black12, blurRadius: 5)
                              ]),
                          child: TextFormField(
                            onSaved: (e) => password = e,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              icon: Icon(
                                Icons.vpn_key,
                                color: Colors.grey,
                              ),
                              hintText: 'Password',
                              suffixIcon: IconButton(
                                onPressed: () {
                                  showHide();
                                },
                                icon: Icon(secureText
                                    ? Icons.visibility_off
                                    : Icons.visibility),
                              ),
                            ),
                            obscureText: secureText,
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 16, right: 32),
                            child: Text(
                              'Forgot Password ?',
                              style: TextStyle(color: Colors.grey),
                            ),
                          ),
                        ),
                        Spacer(),
                        GestureDetector(
                          child: Container(
                            height: 45,
                            width: MediaQuery.of(context).size.width / 1.2,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: [
                                    Color(0xFFf45d27),
                                    Color(0xFFf5851f)
                                  ],
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50))),
                            child: Center(
                              child: Text(
                                'Login'.toUpperCase(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          onTap: () {
                            FocusScope.of(context).requestFocus(FocusNode());
                            check();
                          },
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                  Center(
                    child: Text("Intergrated ERP SAP"),
                  ),
                  Center(
                    child: Text("Copyright 2019"),
                  )
                ],
              )),
            ),
          ),
        );
        break;
      case LoginStatus.signIn:
        return Welcome();
      default:
    }
  }
}
