// import 'dart:collection';
import 'dart:convert';
import 'dart:math';

import 'package:animator/animator.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:hr_management_deploy/bloc/user-bloc.dart';
import 'package:hr_management_deploy/model/UserModel.dart';
import 'package:hr_management_deploy/model/gridmodelmenu.dart';
import 'package:hr_management_deploy/model/messagemodel.dart';
import 'package:hr_management_deploy/page/content/approve.dart';
import 'package:hr_management_deploy/page/content/grafik.dart';
import 'package:hr_management_deploy/page/content/tambah-planing.dart';
import 'package:hr_management_deploy/page/content/welcome.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'content/daftar-planing.dart';
import 'home.dart';

class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  bool status_menu = false;
  String golongan = "";

  Widget angka = Text("");

  getPrev() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      var value = preferences.getInt("value");
      print(value);
    });
  }

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final List<Message> pesandp = [];
  final List<Message> pesanap = [];
  int valpesandp = 0;
  int valpesanapp = 0;
  getPesanDP() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      valpesandp = preferences.getInt("pesandp");
      print(valpesandp);
    });
  }

  getPesanAPP() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      valpesanapp = preferences.getInt("pesanapp");
      print(valpesanapp);
    });
  }

  getApiToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idToken = preferences.getString("idToken");
      print("id token $idToken");
    });
  }

  void handleRouting(dynamic notification) {
    print(notification['title']);
    switch (notification['title']) {
      case 'approve':
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => ApprovePage()));
        break;
      case 'detail':
        Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => DPPage()));
        break;
      default:
        Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => DPPage()));
    }
  }

  savePesanDP(int value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("pesandp", value);
      preferences.commit();
    });
  }

  savePesanAPP(int value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("pesanapp", value);
      preferences.commit();
    });
  }

  String pesan = "";
  UserBlock userBlock;
  var valve = false;
  void initState() {
    // TODO: implement initState
    super.initState();
    valve = false;
    userBlock = Provider.of<UserBlock>(context, listen: false);

    getPrev();
    getPesanDP();
    getPesanAPP();

    status_menu = false;
    golongan = "";
    getApiToken();
    _firebaseMessaging.getToken();
    _firebaseMessaging.subscribeToTopic(userBlock.detailuser.id_user.toString());
    print("ini ${userBlock.detailuser.id_user}");
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        //     _scaffoldKey.currentState.showSnackBar(SnackBar(
        //   content: Text("Selamat Datang ",style: TextStyle(color: Colors.blue),),

        // ));
        // saat aplikasi di buka
        print("onMessage:$message");

        final notification = message['data'];
        // print('on message'+notification['title']);
        // userBlock.detailuser;
        setState(() {
          if (notification['title'] == 'detail') {
            pesandp.add(Message(notification['title'], notification['body']));
            savePesanDP(pesandp.length);
            getPesanDP();
            // print("masuk set detail");
          } else if (notification['title'] == 'approve') {
            pesanap.add(Message(notification['title'], notification['body']));
            savePesanAPP(pesanap.length);
            getPesanAPP();
            // print(pesanap.length);
            // print("masuk set approvel $pesanap.length");
          }
          pesan = "ada pesan nih";
          _getGridItemList();
          build(context);
        });
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onlaunch:$message");
      },
      onResume: (Map<String, dynamic> message) async {
        final notification = message['data'];
        print(notification['title']);

        handleRouting(notification);
        print("onResume: $message");
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
  }

  List<GridModel> _getGridItemList() {
    // print("ini all ${userBlock.detailuser.allaksi}");
    List<GridModel> list = new List<GridModel>();
    // bool status = true;
    if (golongan == "a") {
      list.add(new GridModel("assets/tptp.png", "Membuat\n Surat Ijin", 0));
      list.add(new GridModel(
          "assets/ttd.png",
          "Mempersetujui \n Surat Ijin",
          (valpesanapp != null)
              ? userBlock.detailuser.allaksi+valpesanapp
              : userBlock.detailuser.allaksi));
      list.add(new GridModel("assets/dp.png", "Daftar\n Surat Ijin",
          (valpesandp != null) ? valpesandp : pesandp.length));
      
      // list.add(new GridModel("assets/qr-code.png", "Scan\n QR Otorisasi", 0));
      // list.add(new GridModel("assets/graph.png", "Grafik\n Performa Absen", 0));
      // list.add(new GridModel("assets/dl.png", "Daftar Lembur", 0));
    } else if (golongan == "b") {
      list.add(new GridModel("assets/tptp.png", "Membuat\n Surat Ijin", 0));
      list.add(new GridModel("assets/dp.png", "Daftar\n Surat Ijin",
          (valpesandp != null) ? valpesandp : pesandp.length));
      // list.add(new GridModel("assets/qr-code.png", "Scan\n QR Otorisasi", 0));
      // list.add(new GridModel("assets/graph.png", "Grafik\n Performa Absen", 0));
      // list.add(new GridModel("assets/dl.png", "Daftar Lembur", 0));
    } else if (golongan == "c") {
      list.add(new GridModel("assets/tptp.png", "Membuat\n Surat Ijin", 0));
      list.add(new GridModel("assets/dp.png", "Daftar\n Surat Ijin",
          (valpesandp != null) ? valpesandp : pesandp.length));
      // list.add(new GridModel("assets/qr-code.png", "Scan\n QR Otorisasi", 0));
      // list.add(new GridModel("assets/graph.png", "Grafik\n Performa Absen", 0));
      // list.add(new GridModel("assets/dl.png", "Daftar Lembur", 0));
    }

    return list;
  }

  @override
  Widget build(BuildContext context) {
    // userBlock.idArea;
    // userBlock.idToken;
    // userBlock.fetchKasubag();
    if(valve ==false){
      userBlock.getDetail(userBlock.idToken);
      setState(() {
       valve=true; 
      });
    }
    if (userBlock.detailuser != null) {
      if (userBlock.detailuser.id_jabatan == 202 ||
          userBlock.detailuser.id_jabatan == 203) {
        setState(() {
          golongan = "a";
        });
      } else if (userBlock.detailuser.id_jabatan == 301) {
        setState(() {
          golongan = "b";
        });
      } else {
        setState(() {
          golongan = "c";
        });
      }
    }
    if (userBlock.detailuser != null) {
      return Scaffold(
        body: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 10),
                  width: MediaQuery.of(context).size.width,
                  height: (MediaQuery.of(context).orientation ==
                          Orientation.portrait)
                      ? 220
                      : MediaQuery.of(context).size.height / 1.8,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Color(0xFFf45d40), Color(0xFFf5851f)],
                    ),
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(32),
                        bottomLeft: Radius.circular(32)),
                  ),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 50.0,
                                
                                top: 10.0,
                                bottom: 10.0),
                            child: Container(
                              width:120,
                              height: 120,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    image: NetworkImage((userBlock
                                                .detailuser.file_url !=
                                            null)
                                        ? "http://dash.ptpnix.co.id/hrsistems/public/profil/${userBlock.detailuser.file_url}"
                                        : "https://www.shareicon.net/data/2016/09/01/822739_user_512x512.png"),
                                    fit: BoxFit.fill,
                                  )),
                              child: Column(),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: Column(
                              children: <Widget>[
                                Center(
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        "${userBlock.detailuser.nama_pegawai}",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Icon(
                                        Icons.supervised_user_circle,
                                        color: Colors.white,
                                      ),
                                      Text(
                                        '${userBlock.detailuser.nama_bagian}',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 13.0),
                                      ),
                                      Text(
                                        'ID :${userBlock.detailuser.username}',
                                        style: TextStyle(color: Colors.white),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      // SizedBox(height:MediaQuery.of(context).size.height / 3 ,),
                      Padding(
                        padding: const EdgeInsets.only(left: 16, right: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Animator(
                                  duration: Duration(seconds: 2),
                                  builder: (anim) => Center(
                                    child: Transform.scale(
                                      scale: anim.value,
                                      child: Icon(
                                        Icons.group,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                                Text(
                                  'Cuti Tahunan',
                                  style: TextStyle(color: Colors.white),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  "${userBlock.detailuser.cuti_tahunan}",
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Animator(
                                  duration: Duration(seconds: 2),
                                  builder: (anim) => Center(
                                    child: Transform.scale(
                                      scale: anim.value,
                                      child: Icon(
                                        Icons.stop_screen_share,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                                Text(
                                  'Cuti Panjang',
                                  style: TextStyle(color: Colors.white),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  "${userBlock.detailuser.cuti_panjang}",
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Animator(
                                  duration: Duration(seconds: 2),
                                  builder: (anim) => Center(
                                    child: Transform.scale(
                                      scale: anim.value,
                                      child: Icon(
                                        Icons.block,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                                Text(
                                  'Tanpa Catatan',
                                  style: TextStyle(color: Colors.white),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  "${userBlock.detailuser.jumlahtanpacatatan}",
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Animator(
                                  duration: Duration(seconds: 2),
                                  builder: (anim) => Center(
                                    child: Transform.scale(
                                      scale: anim.value,
                                      child: Icon(
                                        Icons.local_hospital,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                                Text(
                                  'Sakit',
                                  style: TextStyle(color: Colors.white),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  "${userBlock.detailuser.jumlahsakit}",
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 5.0),
                Text(
                  "-- Menu --",
                  style: TextStyle(fontWeight: FontWeight.w100, fontSize: 12),
                ),
                GridView.count(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  crossAxisCount: (MediaQuery.of(context).orientation ==
                          Orientation.portrait)
                      ? 2
                      : 4,
                  children: List<GridItem>.generate(_getGridItemList().length,
                      (int index) {
                    return GridItem(_getGridItemList()[index]);
                  }),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 5),
                Text("HR-MANAGEMENT \nversion 1.0",textAlign: TextAlign.center,)
              ],
            )
          ],
        ),
      );
    } else {
      return Scaffold(
        body: Welcome(),
      );
    }
  }
}

class GridItem extends StatelessWidget {
  GridModel gridModel;

  GridItem(this.gridModel);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: GestureDetector(
          onTap: () {
            switch (gridModel.title) {
              case "Membuat\n Surat Ijin":
                // (userBlock.listkasubag == null)?userBlock.fetchKasubag():print("kasubga berhasil di ambil");
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext) => FormTPPage()));
                break;
              case "Daftar\n Surat Ijin":
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (BuildContext) => DPPage()));
                break;
              case "Mempersetujui \n Surat Ijin":
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext) => ApprovePage()));
                break;
              case "Grafik\n Performa Absen":
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext) => GrafikPage()));
                break;
              default:
            }
          },
          child: Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(32),
                        bottomLeft: Radius.circular(32)),
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    elevation: 4.0,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 100,
                            height: 100,
                            decoration: BoxDecoration(shape: BoxShape.circle),
                            child: Image.asset(
                              gridModel.imagePath,
                              fit: BoxFit.cover,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Text(
                              gridModel.title,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 17),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              (gridModel.angka > 0)
                  ? Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.red),
                      child: Center(
                        child: Text(
                          gridModel.angka.toString(),
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    )
                  : Container(),
            ],
          )),
    );
  }
}
