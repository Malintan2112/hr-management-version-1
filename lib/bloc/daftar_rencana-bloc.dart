import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:hr_management_deploy/model/DaftarRencanaModel.dart';
import 'package:hr_management_deploy/page/content/daftar-planing.dart';
import 'package:http/http.dart' as http;

class DaftarRencanaBlock extends ChangeNotifier {
  List<DaftarRencana> _daftarRencana;
  List<DaftarRencana> get daftarRencana => _daftarRencana;
  set daftarRecana(List<DaftarRencana> val) {
    _daftarRencana = val;
    notifyListeners();
  }

  String _idToken;
  String get idToken => _idToken;
  set idToken(String val) {
    print(" ini val $val");
    _idToken = val;
  }

  Future deletRencana(int idMa, BuildContext context,
      GlobalKey<ScaffoldState> scafoldKey) async {
    try {
      final response = await http.get(
          'http://dash.ptpnix.co.id/hrsistems/api/deletemetaabsenmobile?api_token=${idToken}&idmeta=$idMa');
      if (response.statusCode == 200) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (BuildContext context) => DPPage()));
      } else {
        // scafoldKey.currentState.showSnackBar(SnackBar(
        //   content: Text("Ada gangguang ..."),
        // ));
        print("erorr hapus");
      }
    } on Exception catch (error) {
      print('erorr');
    }
  }

  Future<List<DaftarRencana>> fetchDaftarRencana() async {
    // print(idToken);
    var url =
        'http://dash.ptpnix.co.id/hrsistems/api/plannerpagemobile?api_token=${idToken}';
    print(url);
    try {
      final response = await http.get(url);
      List res = jsonDecode(response.body);

      List<DaftarRencana> data = [];
      for (var i = 0; i < res.length; i++) {
        var post = DaftarRencana.fromJson(res[i]);
        data.add(post);
      }
      daftarRecana = data;
      // print("ambil kasubag lewat bloc");
      print("ambil rencana");

      return daftarRencana;
    } on Exception catch (error) {
      print('erorr');
    }
  }
}
