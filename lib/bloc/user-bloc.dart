import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:hr_management_deploy/model/KasubagModel.dart';
import 'package:hr_management_deploy/model/UserModel.dart';
import 'package:http/http.dart' as http;

class UserBlock extends ChangeNotifier {
  List<Kasubag> _kasubag;
  List<Kasubag> get listkasubag => _kasubag;
  set listkasubag(List<Kasubag> val) {
    _kasubag = val;
    notifyListeners();
  }

  int _idArea;
  int get idArea => _idArea;
  set idArea(int val) {
    _idArea = val;
  }

  String _idToken;
  String get idToken => _idToken;
  set idToken(String val) {
    _idToken = val;
  }

  User _detail;
  User get detailuser => _detail;
  set detailuser(User val) {
    _detail = val;
    notifyListeners();
  }

  List<User> _user;
  List<User> get listuser => _user;
  set listuser(List<User> val) {
    _user = val;
    notifyListeners();
  }

  Future<List<Kasubag>> fetchKasubag() async {
    final response = await http.get(
        'http://dash.ptpnix.co.id/hrsistems/api/getsubarea?api_token=${idToken}&id_area=${idArea}');
    try {
      List res = jsonDecode(response.body);
     
      List<Kasubag> data = [];
      for (var i = 0; i < res.length; i++) {
        var post = Kasubag.fromJson(res[i]);
        data.add(post);
      }
      listkasubag = data;
      print("ambil kasubag lewat bloc");
      return listkasubag;
    } on Exception catch (error) {
      print(idToken);
      print('kasubag error');
    }
  }

  Future<List<User>> fetchPost() async {
    final response = await http.get(
        'http://dash.ptpnix.co.id/hrsistems/api/getdatauser?api_token=\$2y\$10\$e0ZWK8yfF.WlSXqj/LnJ8.TBJSnuzHXbPr94j74e8/LdfkrObnPQq');

    // if (response.statusCode == 200) {
    //   // If the call to the server was successful, parse the JSON.
    //   return User.fromJson(json.decode(response.body));
    // } else {
    //   // If that call was not successful, throw an error.
    //   throw Exception('Failed to load post');
    // }
    List res = jsonDecode(response.body);
    List<User> data = [];
    for (var i = 0; i < res.length; i++) {
      var post = User.fromJson(res[i]);
      data.add(post);
    }
    listuser = data;
    return listuser;
  }

  Future<User> getDetail(String idToken) async {
    try {
      final response = await http.get(
          'http://dash.ptpnix.co.id/hrsistems/api/getdatauser?api_token=' +
              idToken);
      detailuser = User.fromJson(jsonDecode(response.body));
      print('ambil data');
      return detailuser;
    } on Exception catch (error) {
      print('erorr');
    }
  }
}
