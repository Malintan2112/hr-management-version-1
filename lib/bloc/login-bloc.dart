
import 'dart:async';

import 'package:rxdart/rxdart.dart';

mixin  Validators{
  var nikValidator = StreamTransformer<String,String>.fromHandlers(
    handleData:  (nik,sink){
      if(nik.length==7){
        sink.add(nik);
      }else{
        sink.addError("Tidak sesuai dengan format NIK !");
      }
    }
  );
  var passwordValidator = StreamTransformer<String,String>.fromHandlers(
    handleData:  (password,sink){
      if(password.length>4){
        sink.add(password);
      }else{
        sink.addError("Password lengh should be greater than  4 chara.");
      }
    }
  ); 
}
class LoginBloc extends Object with Validators implements BaseBloc{
  final _nikController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();

  //keran yg berfungsi untuk memasukan data 
  StreamSink<String> get nikChanged => _nikController.sink;
  StreamSink<String> get passwordChanged => _passwordController.sink; 

  //berfungsi untuk memproses data dari keran dan melakukan validasi 
  Stream<String> get nik=>_nikController.stream.transform(nikValidator);
  Stream<String> get password=>_passwordController.stream.transform(passwordValidator);

  //memastikan bahwa kombinasi data berhasil dengan baik
  Stream<bool> get submitCheck => Observable.combineLatest2(nik,password,(e,p)=>true);

  @override
  void dispose() {
    // TODO: implement dispose
    _nikController?.close();
    _passwordController?.close();
  }

}

abstract class BaseBloc{
void dispose();
}