import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:hr_management_deploy/model/ApproveRencanaModel.dart';
import 'package:hr_management_deploy/page/content/approve.dart';
import 'package:http/http.dart' as http;

class ApproveRencanaBlock extends ChangeNotifier {
  List<ApproveRencana> _approveRencana;
  List<ApproveRencana> get approveRencana => _approveRencana;
  set approveRencana(List<ApproveRencana> val) {
    _approveRencana = val;
    notifyListeners();
  }

  String _idToken;
  String get idToken => _idToken;
  set idToken(String val) {
    _idToken = val;
  }

  Future cancelRencana(int idMa, BuildContext context,
      GlobalKey<ScaffoldState> scafoldKey) async {
    try {
      final response = await http.get(
          'http://dash.ptpnix.co.id/hrsistems/api/cancelmetaabsenmobile?api_token=${idToken}&idmeta=$idMa');
      if (response.statusCode == 200) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => ApprovePage()));
      } else {
        // scafoldKey.currentState.showSnackBar(SnackBar(
        //   content: Text("Ada gangguang ..."),
        // ));
        print("erorr cancel");
      }
    } on Exception catch (error) {
      print('erorr');
    }
  }

  Future approveRencanaMA(int idMa, int id_jabatan,BuildContext context,
      GlobalKey<ScaffoldState> scafoldKey) async {
    try {
      final response = await http.get(
          'http://dash.ptpnix.co.id/hrsistems/api/approvemetaabsenmobile?api_token=${idToken}&idmeta=$idMa&hakakses=$id_jabatan');
      if (response.statusCode == 200) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => ApprovePage()));
      } else {
        // scafoldKey.currentState.showSnackBar(SnackBar(
        //   content: Text("Ada gangguang ..."),
        // ));
        print("erorr approve");
      }
    } on Exception catch (error) {
      print('erorr');
    }
  }

  Future<List<ApproveRencana>> fetchApproveRencana() async {
    print("ini id recana $idToken");
    try {
      final response = await http.get(
          'http://dash.ptpnix.co.id/hrsistems/api/aksipresensimobile?api_token=$_idToken');
      List res = jsonDecode(response.body);
      print("ambil approve rencana");
      //
      // final items = json.decode(response.body).cast<Map<String, dynamic>>();
      // List<Kasubag> listOfKasubag = items.map<Kasubag>((json) {
      //   return Kasubag.fromJson(json);
      // }).toList();
      //
      List<ApproveRencana> data = [];
      for (var i = 0; i < res.length; i++) {
        var post = ApproveRencana.fromJson(res[i]);
        data.add(post);
      }
      approveRencana = data;
      // print("ambil kasubag lewat bloc");
      return approveRencana;
    } on Exception catch (error) {
      print('erorr');
    }
  }
}
