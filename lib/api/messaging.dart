import 'dart:convert';

import 'package:http/http.dart';
class Messaging{
  static final Client client = Client();

  static const String serverKey= "AAAA5486ZGc:APA91bG7Qq5L9oYZWWnWLQggb4NOjXc5EUyjQStCWgaWIhUH5V-NgcVguJ6LMamWq14yk8qn1XEcuNyGHqV_8bk7rnIPKLOa24e6x5iO05Gw7PdhOKw-S6m7tIHxs5FAayFSmqeXj1SH";
  
  static Future<Response>sendToAll({
    String title, String body,
  })=>sendToTopic(title: title,body: body,topic: 'all');

  static Future<Response>sendToTopic({
    String title, String body , String topic
  })=>sendTo(title: title,body: body,fcmToken:'/topics/$topic' );
  
  
  static Future<Response> sendTo({
    String title, String body , String fcmToken
  })=>client.post(
      'https://fcm.googleapis.com/fcm/send',body: jsonEncode(
        {
          'notification':{'body':'$body','title':'$title'},
          'priority':'high',
          'data':{
            'click_action':'FLUTTER_NOTIFICATION_CLICK',
            'id':'1',
            'status':'done'
          },
          'to':'$fcmToken'
      }),
      headers: {
        'Content-Type':'application/json',
        'Authorization':'key=$serverKey'
      }
  );
}