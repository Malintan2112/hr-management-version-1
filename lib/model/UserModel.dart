class User {
  final int id_user;
  final String nik;
  final String nama_pegawai;
  final String username;
  final int id_jabatan;
  final String nama_jabatan;
  final String golongan;
  final int id_area;
  final int noabsen;
  final String nama_bagian;
  final int id_subarea;
  final String nama_subbagian;
  final String status;
  final String file_url;
  final String nama_kabag;
  final String nama_kasubag;
  final int jumlahmasuk;
  final int jumlahterlambat;
  final int jumlahdinas;
  final int jumlahcuti;
  final int julahlembur;
  final int jumlahdispensasi;
  final int jumlahsakit;
  final int jumlahtanpacatatan;
  final int tidakhadir;
  final int cuti_tahunan;
  final int cuti_panjang;
  final int id_kabag;
  final String id_token;
  final int allaksi;
  User(
      this.id_user,
      this.nik,
      this.nama_pegawai,
      this.username,
      this.id_jabatan,
      this.nama_jabatan,
      this.golongan,
      this.id_area,
      this.nama_bagian,
      this.id_subarea,
      this.nama_subbagian,
      this.status,
      this.file_url,
      this.nama_kabag,
      this.nama_kasubag,
      this.jumlahmasuk,
      this.jumlahterlambat,
      this.jumlahdinas,
      this.jumlahcuti,
      this.julahlembur,
      this.jumlahdispensasi,
      this.jumlahsakit,
      this.jumlahtanpacatatan,
      this.tidakhadir,
      this.cuti_tahunan,
      this.cuti_panjang, this.noabsen, this.id_kabag, this.id_token, this.allaksi);
  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        json['id_users'],
        json['nik'],
        json['nama_pegawai'],
        json['username'],
        json['id_jabatan'],
        json['nama_jabatan'],
        json['golongan'],
        json['id_area'],
        json['nama_bagian'],
        json['id_subarea'],
        json['nama_subbagian'],
        json['status'],
        json['file_url'],
        json['nama_kabag'],
        json['nama_kasubag'],
        json['jumlahmasuk'],
        json['jumlahterlambat'],
        json['jumlahdinas'],
        json['jumlahcuti'],
        json['jumlahlembur'],
        json['jumlahdispensasi'],
        json['jumlahsakit'],
        json['jumlahtanpacatatan'],
        json['tidakhadir'],
        json['cuti_tahunan'],
        json['cuti_panjang'],
        json['noabsen'],
        json['id_kabag'],
        json['api_token'],
        json['allaksi']
        );
  }
}
