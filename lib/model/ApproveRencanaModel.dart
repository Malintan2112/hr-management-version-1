class ApproveRencana {
  final int id_user;
  final int id_ma;
  final int status;
  final String tanggal_awal;
  final String desc_keterangan;
  final int approved_status;
  final String approved_people;
  final String name;
  final String file_url;
  final String evidence;
  final String ektension;
  ApproveRencana(
      this.id_user,
      this.id_ma,
      this.status,
      this.tanggal_awal,
      this.desc_keterangan,
      this.approved_status,
      this.approved_people,
      this.name,
      this.file_url,
      this.evidence, this.ektension);
  factory ApproveRencana.fromJson(Map<String, dynamic> json) {
    return ApproveRencana(
        json['id_user'],
        json['id_ma'],
        json['status'],
        json['tanggal_awal'],
        json['desc_keterangan'],
        json['approved_status'],
        json['approved_people'],
        json['name'],
        json['file_url'],
        json['evidence'],json['extension']);
  }
}
