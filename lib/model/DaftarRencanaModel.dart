class DaftarRencana {
  final int id_ma;
  final int id_user;
  final int noabsen;
  final String tanggal_awal;
  final String tanggal_akhir;
  final String desc_keterangan;
  final String approved_people;
  final int status;
  final int approved_status;
  final String date_input;
  

  DaftarRencana(
      this.id_ma,
      this.id_user,
      this.noabsen,
      this.tanggal_awal,
      this.tanggal_akhir,
      this.desc_keterangan,
      this.approved_people,
      this.status,
      this.approved_status,
      this.date_input,
    );
  factory DaftarRencana.fromJson(Map<String, dynamic> json) {
    return DaftarRencana(json['id_ma'], json['id_user'], json['noabsen'], json['tanggal_awal'], json['tanggal_akhir'],
        json['desc_keterangan'], json['approved_people'], json['status'], json['approved_status'], json['date_input']);
  }

  // Kasubag(this.nama_kasubag, this.id_kasubag);
  // factory Kasubag.fromJson(Map<String, dynamic> json) {
  //   return Kasubag(json['nama_kasubag'],json['id_kasubag']);
  // }
}
