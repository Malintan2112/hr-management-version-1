import 'package:flutter/material.dart';

class GridModel {
  String _imagePath;
  String _title;
  int _angka;

  GridModel(this._imagePath, this._title, this._angka);

  int get angka => _angka;
  set angka(int value) {
    _angka = value;
  }

  String get title => _title;
  set title(String value) {
    _title = value;
  }

  String get imagePath => _imagePath;
  set imagePath(String value) {
    _imagePath = value;
  }
}
