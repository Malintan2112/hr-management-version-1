class Kasubag{
  final String nama_kasubag;
  final int id_kasubag;

  Kasubag(this.nama_kasubag, this.id_kasubag);
  factory Kasubag.fromJson(Map<String, dynamic> json) {
    return Kasubag(json['nama_kasubag'],json['id_kasubag']);
  }
}