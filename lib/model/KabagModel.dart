class Kabag{
  final String nama_kabag;
  final int id_kabag;

  Kabag(this.nama_kabag, this.id_kabag);
  factory Kabag.fromJson(Map<String, dynamic> json) {
    return Kabag(json['nama_kabag'],json['id_kabag']);
  }
}